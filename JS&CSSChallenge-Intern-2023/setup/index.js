const previewSection = document.querySelector('.preview');
const layoutPlaceholder = document.querySelector('.layout-placeholder');

const loadMoreBtn = document.createElement('button');
loadMoreBtn.classList.add('loadmore');
loadMoreBtn.innerText = 'Load More';
previewSection.append(loadMoreBtn);
const cards = document.createElement('div');
cards.classList.add('cards', 'flex');

// settings
const numberOfColumns = document.querySelector('#numberOfColumns');
const backgroundColorSelector = document.querySelector('#cardBackgroundColor');
const cardSpaceBetween = document.querySelector('#cardSpaceBetween');

const radioGroup = document.querySelector('radio-group');
const allFilter = document.querySelector('#all');
const instagramFilter = document.querySelector('#instagram');
const facebookFilter = document.querySelector('#facebook');
const twitterFilter = document.querySelector('#twitter');

const radio = document.querySelector('.radio');
const darkTheme = document.querySelector('#darkTheme');
const lightTheme = document.querySelector('#lightTheme');

fetch('../data.json')
  .then((response) => response.json())

  .then((data) => {
    data.forEach((post) => {
      let isLike = false;
      const fullDate = new Date(post.date);
      const day = new Date(post.date).getDay();
      const month = fullDate.toLocaleString('default', { month: 'short' });
      const year = new Date(post.date).getFullYear();

      const date = day + ' ' + month + ' ' + year;
      cards.innerHTML;

      const card = document.createElement('div');
      const innerCard = document.createElement('div');
      innerCard.classList.add('card-inner');
      card.classList.add('card');
      card.classList.add(post.source_type);
      innerCard.innerHTML += `
        
        <div class="header flex">
          <div class="profile flex">
            <img
              class="profilePic"
              src=${post.profile_image}
            />
            <div class="info">
              <p class="name">${post.name}</p>
              <p class="date">${date}</p>
            </div>
          </div>
          <img class="social" src=${
            post.source_type === 'instagram'
              ? '../icons/instagram-logo.svg'
              : '../icons/facebook.svg'
          } />
        </div>
        <img class="img" src=${post.image} />
        <p class="caption">
         ${post.caption}
        </p>
        
      
          `;

      const cardFooter = document.createElement('div');
      const heart = document.createElement('i');
      const likes = document.createElement('p');

      cardFooter.classList.add('cardFooter', 'flex');
      heart.classList.add('fa-regular', 'fa-heart', 'heart');
      likes.classList.add('likes');

      likes.innerText = post.likes;

      cardFooter.append(heart, likes);
      innerCard.append(cardFooter);
      card.append(innerCard);
      cards.append(card);

      heart.addEventListener('click', (e) => {
        isLike = !isLike;
        if (isLike) {
          heart.classList.remove('fa-regular');
          heart.classList.add('fa-solid', 'heartGrow');
          likes.innerText = +post.likes + 1;
        } else {
          heart.classList.remove('fa-solid', 'heartGrow');
          heart.classList.add('fa-regular');
          likes.innerText = +likes.innerText - 1;
        }
      });

      //settings

      backgroundColorSelector.addEventListener('input', (e) => {
        innerCard.style.backgroundColor = e.target.value;
      });

      numberOfColumns.addEventListener('input', (e) => {
        if (e.target.value === 'dynamic') {
          card.style.flexBasis = '20%';
        } else {
          card.style.flexBasis = `${100 / e.target.value}%`;
        }
      });

      cardSpaceBetween.addEventListener('input', (e) => {
        innerCard.style.margin = e.target.value;
      });

      darkTheme.addEventListener('change', () => {
        lightTheme.checked = false;
        darkTheme.checked = true;
        if (darkTheme.checked) {
          innerCard.style = 'background-color:black;color:white';
        }
      });
      lightTheme.addEventListener('change', () => {
        lightTheme.checked = true;
        darkTheme.checked = false;
        if (lightTheme.checked) {
          innerCard.style = 'background-color:none;color:black';
        }
      });
      // allFilter.addEventListener('change', () => {
      //   card.style.display = 'none';
      //   card.style.display = 'block';
      // });
      // instagramFilter.addEventListener('change', () => {
      //   const instaCard = document.querySelectorAll('.instagram');
      //   card.style.display = 'none';
      //   instaCard.style.display = 'block';
      // });
    });

    layoutPlaceholder.append(cards);
  });

window.addEventListener('click', () => {
  const card = document.querySelectorAll('.card');

  let currentItem = 4;
  loadMoreBtn.addEventListener('click', function (e) {
    for (let i = currentItem; i < currentItem + 4; i++) {
      if (card[i]) {
        card[i].style.display = 'block';
      }
    }
    currentItem += 4;
    if (currentItem >= card.length) {
      e.target.style.display = 'none';
    }
  });
});
